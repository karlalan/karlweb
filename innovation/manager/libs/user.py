from django import forms
from datetime import datetime as dt

TODAY = dt.now()

class Register(forms.Form):
    userid = forms.CharField(max_length = 50)
    email = forms.CharField(max_length = 100)
    password = forms.CharField(max_length = 50)
    retype_password = forms.CharField(max_length = 50)

    '''
    Clean registration form and make error message if invalid
    required information exists.
    '''
    def clean(self):
        cd = self.cleaned_data
        if cd.get("password") != cd.get("retype_password"):
            self.add_error("retype_password", "Passwords do not match !")


class Login(forms.Form):
    userid = forms.CharField()
    password = forms.CharField()
