from enum import Enum, auto

class SysRequest(Enum):
    POST = auto()
    GET = auto()

    def GetName(self) -> str: return self.name
