

class Util():
    '''
    This method can be applied on both numeric and string.
    '''
    @staticmethod
    def IsNumber(data) -> bool:
        IsNum = False
        if Util.IsInt(data) or Util.IsFloat(data): IsNum = True
        else:
            if Util.IsString(data):
                try:
                    data = float(data)
                    IsNum = True
                except Exception as e:
                    ExMes = "There are some strings included."
        return IsNum
    '''
    This method only can be applied on string.
    '''
    @staticmethod
    def IsStrNumber(data: str) -> bool:
        if not Util.IsString(data): return False
        import re
        STR_NUM_PATTERN = r"^(-?)(\d+)(.?\d+)$"
        return True if re.match(STR_NUM_PATTERN, data) else False

    @staticmethod
    def IsInt(data) -> bool: return type(data) == int

    @staticmethod
    def IsFloat(data) -> bool: return type(data) == float

    @staticmethod
    def IsString(data) -> bool: return type(data) == str


