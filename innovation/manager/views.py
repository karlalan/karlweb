from django.shortcuts import render
from django.views import View
import django.http
import manager.models
from manager.libs.user import Register, Login

'''
Home page dispatch
'''
class Home(View):
    def get(self, request):
        data = {}
        return render(request, "index.html", data)

'''
User page dispatch
'''
class User(View):
    data = {}

    def get(self, request):
        return render(request, "user/register.html", self.data)

    def post(self, request):
        form = Register(request.POST)
        ## check required parameters
        if form.is_valid():
            self.data["ErrMes"] = form
        else:
            self.data["ErrMes"] = form.errors
        return render(request, "user/register.html", self.data)