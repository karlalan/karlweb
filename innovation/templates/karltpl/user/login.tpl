{% extends 'common/modal.tpl' %}

{% block ExtraStyle %}
<style type="text/css">
    .glyphicon-user:before {
        content: "\1F464";
    }
    .glyphicon-user {
        margin-right: 20px;
        margin-top: 20px;
    }
    .glyphicon-lock:before {
        content: "\1F512";
    }
    .glyphicon-lock {
        margin-right: 20px;
        margin-top: 73px;
    }
    .has-feedback .form-control {
        padding-right: 42.5px;
    }
    .form-control-feedback {
        position: absolute;
        top: 0;
        right: 0;
        z-index: 2;
        display: block;
        width: 34px;
        height: 34px;
        line-height: 34px;
        text-align: center;
        pointer-events: none;
        color: #777;
    }
    .register-link {
        position: relative;
        left: -112px;
    }
</style>
{% endblock %}

{% block ModalTitle %}
Please sign in to start your session!
{% endblock %}

{% block ModalBody %}
<div class="form-group has-feedback">
    <input type="text" class="form-control" placeholder="UserID">
    <span class="glyphicon glyphicon-user form-control-feedback"></span>
</div>
<div class="form-group has-feedback">
    <input type="password" class="form-control" placeholder="Password">
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
</div>
<div class="row">
    <div class="col-xs-8" style="margin-left: 15px;">
        <div class="checkbox icheck">
            <label>
                <input type="checkbox"> Remeber me
            </label>
        </div>
    </div>
    <!-- /.col -->
</div>
{% endblock %}

{% block ModalFooter %}
<a href="{% url 'register' %}" class="register-link">Register a new memebership</a>
<button type="button" class="btn btn-success" data-dismiss="modal">
    Sign in
</button>
{% endblock %}