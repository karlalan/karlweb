{% load static %}
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{% static 'img/icon/favicon.ico' %}">

    <title>Carousel Template for Bootstrap</title>

    <script src="{% static 'js/jQuery-3.2.1.min.js' %}"></script>

    <!-- Bootstrap core CSS -->
    <link href="{% static 'css/bootstrap.min.css' %}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{% static 'css/carousel.css' %}" rel="stylesheet">
</head>