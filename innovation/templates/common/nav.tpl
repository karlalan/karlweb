<nav class="nav blog-nav">
    <a class="nav-link active" href="#">Home</a>
    <a class="nav-link" href="#">ToDo</a>
    <a class="nav-link" href="#">Schedule</a>
    <a class="nav-link" href="#">Info</a>
    <a class="nav-link" href="#">About</a>

    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
      <img src="" class="user-image" alt="User Image">
      <!-- <span class="hidden-xs">Alexander Pierce</span> -->
    </a>
    <ul class="dropdown-menu">
      <!-- User image -->
      <li class="user-header">
        <img src="" class="img-circle" alt="User Image">

        <p>
          Alexander Pierce - Web Developer
          <small>Member since Nov. 2012</small>
        </p>
      </li>
      <!-- Menu Body -->
      <li class="user-body">
        <div class="row">
          <div class="col-xs-4 text-center">
            <a href="#">Followers</a>
          </div>
          <div class="col-xs-4 text-center">
            <a href="#">Sales</a>
          </div>
          <div class="col-xs-4 text-center">
            <a href="#">Friends</a>
          </div>
        </div>
        <!-- /.row -->
      </li>
      <!-- Menu Footer-->
      <li class="user-footer">
        <div class="pull-left">
          <a href="#" class="btn btn-default btn-flat">Profile</a>
        </div>
        <div class="pull-right">
          <a href="#" class="btn btn-default btn-flat">Sign out</a>
        </div>
      </li>
    </ul>
</nav>