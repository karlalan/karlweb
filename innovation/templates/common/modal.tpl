{% block ExtraStyle %}{% endblock %}

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    {% block ModalTitle%}{% endblock %}
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">
                {% block ModalBody %}{% endblock %}
            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                {% block ModalFooter %}{% endblock %}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>