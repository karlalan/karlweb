karlweb
===

## How to start server

[Commands]
```
step 1: open your cmd at your project directory (ex: karlweb for this project)
step 2: $ vagrant up
step 3: $ vagrant ssh
step 4: $ source /env3.6/bin/activate
step 5: $ python /vagrant/innovation/manage.py creatsuperuser
step 6: $ python /vagrant/innovation/manage.py runserver 0.0.0.0:8000
```

## Access your WEB app
```
http://localhost:1209
```