#!/bin/sh
echo '----- Preparation -----'
yum makecache fast
yum update -y
yum install -y yum-utils
yum groupinstall -y development

echo '----- Install Apache -----'
yum install -y httpd httpd-devel
httpd -v

echo '----- Install Python -----'
yum install -y https://centos7.iuscommunity.org/ius-release.rpm
yum install -y python36u python36u-libs python36u-devel
which python
which python3.6

echo '----- Create Python3.6 environment -----'
python3.6 -m venv /env3.6  # python3.6仮想環境はルートに作ることにする。
source /env3.6/bin/activate
which python

# なんか Vagrant では yum で取得する pip に異常があるので別途仮想python3.6環境内へインストール。(ImportError main)
echo '----- Install pip -----'
curl https://bootstrap.pypa.io/get-pip.py -o /get-pip.py
python /get-pip.py
rm /get-pip.py -f
pip install --upgrade pip setuptools
pip install -r /vagrant/requirements.txt

echo '----- Django startup -----'
python /vagrant/innovation/manage.py migrate

echo '----- Auto start -----'
systemctl enable httpd.service